import { Component, OnInit, Input } from '@angular/core';
import { RouteParams } from '@angular/router'
import { DataServiceService } from '../data-service.service';

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styleUrls: ['./all.component.css']
})
export class AllComponent implements OnInit {
    data: any;

  constructor(private dataService: DataServiceService) { 
  	
  }

  ngOnInit() {
  	this.data = this.dataService.getData();
  }

}
