import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { HttpClientJsonpModule } from '@angular/common/http';
import { CommonModule} from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { AddedComponent } from './added/added.component';
import { DeletedComponent } from './deleted/deleted.component';
import { ModifiedComponent } from './modified/modified.component';
import { AllComponent } from './all/all.component';

const appRoutes: Routes = [
  { path: 'added', component: AddedComponent },
  { path: 'deleted', component: DeletedComponent },
  { path: 'modified', component: ModifiedComponent },
  { path: 'all', component: AllComponent },
  { path: '', redirectTo: '/all', pathMatch: 'full'},
  { path: '**', redirectTo: '/all', pathMatch: 'full'}
]

@NgModule({
  declarations: [
    AppComponent,
    AddedComponent,
    DeletedComponent,
    ModifiedComponent,
    AllComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpClientJsonpModule,
    CommonModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
