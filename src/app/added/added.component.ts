import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-added',
  templateUrl: './added.component.html',
  styleUrls: ['./added.component.css']
})
export class AddedComponent implements OnInit {
  @Input() add = '';

  constructor() { }

  ngOnInit() {
  }

}
