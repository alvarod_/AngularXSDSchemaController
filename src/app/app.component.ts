import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpClientJsonpModule } from '@angular/common/http';
import { DataServiceService } from './data-service.service';

@Component({
  selector: 'app-root', 
  providers: [DataServiceService],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  obj = 'XSDSchemaController';
  data = '';
  title = '';
  added = {};
  deleted = {};
  modified = {};
  results = '';
  fileArray = [];
  seleccionado = '';

  fileChange($event){
    console.log('---- ' + this.seleccionado);

    for (var i=0; i< this.data.length; i++) {
        console.log(Object.keys(this.data[i]).toString());
        console.log(this.seleccionado);
        if (Object.keys(this.data[i]).toString() === this.seleccionado ){
          this.title = JSON.stringify(this.data[i]);
          this.dataService.setData(this.title);
        }
        /**Seleccionado vacio mostrar todo**/
        if (this.seleccionado == "Todos los cambios"){
          this.title = JSON.stringify(this.data);
          this.dataService.setData(this.title);
        }
    }
  }

  constructor(private http: HttpClient, private dataService: DataServiceService){
  }
  ngOnInit(): void {
    this.http.get('http://localhost:8080/WebXSDSchemaController/service/VerCambios').subscribe(data => {
      this.title = JSON.stringify(data);
      console.log(this.title);
      var json = this.title, obj = JSON.parse(json);
      console.log(obj);
      console.log(Object.keys(obj));

      for (var i=0; i< obj.length; i++) {
        this.fileArray.push(Object.keys(obj[i]));
      }

      this.data = obj;
    });
  }
}
