import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})

export class DataServiceService {
	data = {};

  constructor() { }

  getData(): any{
  	return this.data;
  }

  setData(data): void{
  	this.data = data;
  }

}
